package quote.infrastructure;

import quote.Application;
import quote.domain.Quote;
import quote.domain.QuoteRepository;
import org.slf4j.LoggerFactory;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.web.client.RestTemplate;

public class RestQuoteRepository implements QuoteRepository {
    private String quoteAPIUrl;

    public RestQuoteRepository(String quoteAPIUrl) {
        this.quoteAPIUrl = quoteAPIUrl;
    }

    @Override
    public Quote fetchRandom() {
        String endpoint = quoteAPIUrl+"/api/random";
        RestTemplate restTemplate = new RestTemplateBuilder().build();
        QuoteDTO dto = restTemplate.getForObject(
                endpoint,
                QuoteDTO.class
        );
        return new Quote(dto.getValue().getQuote());
    }
}
