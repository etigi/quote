package quote.infrastructure;

import quote.domain.IdGenerator;

import java.util.UUID;

public class UUIDGenerator implements IdGenerator {
    @Override
    public String next() {
        return UUID.randomUUID().toString();
    }
}
