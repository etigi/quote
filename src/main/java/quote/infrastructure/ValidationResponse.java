package quote.infrastructure;

public class ValidationResponse {
    public String correctness;
    public String missingword;
    public String fullquote;

    public ValidationResponse(String correctness, String missingword, String fullquote) {
        this.correctness = correctness;
        this.missingword = missingword;
        this.fullquote = fullquote;
    }
}
