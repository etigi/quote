package quote.infrastructure;

import quote.domain.Answer;
import quote.domain.AnswerId;
import quote.domain.AnswerRepository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class PostgresAnswerRepository implements AnswerRepository {
    private Connection connection;

    public PostgresAnswerRepository(Connection connection) {
        this.connection = connection;
    }

    @Override
    public Answer fetchAnswerById(AnswerId id) throws AnswerNotFound {
        try {
            PreparedStatement pstmt = connection.prepareStatement(
                    "SELECT * FROM quote.answer WHERE id = ?"
            );
            pstmt.setString(1, id.id);
            ResultSet set = pstmt.executeQuery();
            if (!set.next()) {
                throw new AnswerNotFound(id);
            } else {
                return new Answer(
                        new AnswerId(set.getString("id")),
                        set.getString("missing_word"),
                        set.getString("full_quote")
                );
            }
        } catch (SQLException e) {
            throw new AnswerNotFound(id);
        }
    }

    @Override
    public void persistAnswer(Answer answer) throws PersistFailure {
        try {
            PreparedStatement pstmt = connection.prepareStatement(
                    "INSERT INTO quote.answer (id, missing_word, full_quote) VALUES (?,?,?)"
            );
            pstmt.setString(1, answer.id.id);
            pstmt.setString(2, answer.missingWord);
            pstmt.setString(3, answer.fullQuote);
            pstmt.executeUpdate();
        } catch (SQLException e) {
            throw new PersistFailure(answer.id);
        }
    }
}
