package quote.infrastructure;

import org.slf4j.LoggerFactory;
import quote.Application;
import quote.domain.AnswerRepository;
import quote.domain.IdGenerator;
import quote.domain.QuoteRepository;
import quote.usecases.GetRandomQuoteWithBlank;
import quote.usecases.ValidateAnswer;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DependencyInjection {
    private String quoteAPIUrl;
    private String dbName;
    private String dbUser;
    private String dbPassword;
    private QuoteRepository quoteRepository;
    private AnswerRepository answerRepository;
    private IdGenerator idGenerator;
    private GetRandomQuoteWithBlank getRandomQuoteWithBlank;
    private ValidateAnswer validateAnswer;
    private Connection connection;

    public QuoteRepository provideQuoteRepository() {
        if (quoteRepository == null) {
            quoteRepository = new RestQuoteRepository(quoteAPIUrl);
        }
        return quoteRepository;
    }

    public AnswerRepository provideAnswerRepository() {
        if (answerRepository == null) {
            answerRepository = new PostgresAnswerRepository(
                    provideConnection()
            );
        }
        return answerRepository;
    }

    public IdGenerator provideIdGenerator() {
        if (idGenerator == null) {
            idGenerator = new UUIDGenerator();
        }
        return idGenerator;
    }

    public GetRandomQuoteWithBlank provideGetRandomQuoteWithBlank() {
        if (getRandomQuoteWithBlank == null) {
            getRandomQuoteWithBlank = new GetRandomQuoteWithBlank(
                    provideQuoteRepository(),
                    provideAnswerRepository(),
                    provideIdGenerator()
            );
        }
        return getRandomQuoteWithBlank;
    }

    public ValidateAnswer provideValidateAnswer() {
        if (validateAnswer == null) {
            validateAnswer = new ValidateAnswer(
                    provideAnswerRepository()
            );
        }
        return validateAnswer;
    }

    private Connection provideConnection() {
        if (connection == null) {
            String dbUrl = "jdbc:postgresql://pc-postgres:5432/"+dbName;
            try {
                connection = DriverManager.getConnection(dbUrl, dbUser, dbPassword);
            } catch (SQLException e) {
                LoggerFactory.getLogger(Application.class).error(e.getMessage(), e);
            }
        }
        return connection;
    }

    public DependencyInjection(String quoteAPIUrl, String dbName, String dbUser, String dbPassword) {
        this.quoteAPIUrl = quoteAPIUrl;
        this.dbName = dbName;
        this.dbUser = dbUser;
        this.dbPassword = dbPassword;
    }
}
