package quote.infrastructure;

public class QuoteResponse {
    public String answerId;
    public String quote;

    public QuoteResponse(String id, String quote) {
        this.answerId = id;
        this.quote = quote;
    }
}
