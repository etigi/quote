package quote.domain;

public class Validation {
    public Correctness correctness;
    public String missingWord;
    public String solution;

    public Validation(Correctness correctness, String missingWord, String solution) {
        this.correctness = correctness;
        this.missingWord = missingWord;
        this.solution = solution;
    }
}
