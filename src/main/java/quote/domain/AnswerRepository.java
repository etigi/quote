package quote.domain;

public interface AnswerRepository {
    Answer fetchAnswerById(AnswerId id) throws AnswerNotFound;
    void persistAnswer(Answer answer) throws PersistFailure;

    class AnswerNotFound extends Exception {
        public AnswerNotFound(AnswerId id) {
            super("Could not find answer with id "+id.id);
        }
    }

    class PersistFailure extends Exception {
        public PersistFailure(AnswerId id) {
            super("Could not save answer with id "+id.id);
        }
    }
}
