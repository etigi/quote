package quote.domain;

public class QuoteWithBlank {
    public AnswerId id;
    public String content;

    public QuoteWithBlank(AnswerId id, String content) {
        this.id = id;
        this.content = content;
    }
}
