package quote.domain;

public interface IdGenerator {
    public String next();
}
