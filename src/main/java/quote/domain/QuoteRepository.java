package quote.domain;

public interface QuoteRepository {
    public Quote fetchRandom();
}
