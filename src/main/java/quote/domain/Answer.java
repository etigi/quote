package quote.domain;

public class Answer {
    public AnswerId id;
    public String missingWord;
    public String fullQuote;

    public Answer(AnswerId id, String missingWord, String fullQuote) {
        this.id = id;
        this.missingWord = missingWord;
        this.fullQuote = fullQuote;
    }
}
