package quote;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import quote.domain.AnswerRepository.AnswerNotFound;
import quote.domain.AnswerRepository.PersistFailure;
import quote.domain.Correctness;
import quote.domain.QuoteWithBlank;
import quote.domain.Validation;
import quote.infrastructure.DependencyInjection;
import quote.infrastructure.QuoteResponse;
import quote.infrastructure.ValidationResponse;
import quote.usecases.GetRandomQuoteWithBlank;
import quote.usecases.ValidateAnswer;

import java.util.Map;

@SpringBootApplication
@RestController
public class Application {
    @Value("${QUOTEURL}")
    private String quoteAPIUrl;

    @Value("${POSTGRES_DB}")
    private String dbName;

    @Value("${POSTGRES_USER}")
    private String dbUser;

    @Value("${POSTGRES_PASSWORD}")
    private String dbPassword;

    @RequestMapping("/")
    public QuoteResponse home() throws PersistFailure {
        DependencyInjection di = new DependencyInjection(
                quoteAPIUrl,
                dbName,
                dbUser,
                dbPassword
        );
        GetRandomQuoteWithBlank usecase = di.provideGetRandomQuoteWithBlank();
        QuoteWithBlank quote = usecase.getRandomQuoteWithBlank();
        return new QuoteResponse(
                quote.id.id,
                quote.content
        );
    }

    @RequestMapping(
            value = "/validate",
            method = RequestMethod.POST
    )
    public ValidationResponse validate(@RequestBody Map<String, Object> payload) throws AnswerNotFound {
        DependencyInjection di = new DependencyInjection(
                quoteAPIUrl,
                dbName,
                dbUser,
                dbPassword
        );
        ValidateAnswer usecase = di.provideValidateAnswer();
        Validation validation = usecase.validateAnswer(
                payload.get("id").toString(),
                payload.get("answer").toString()
        );
        String correctnessMessage = null;
        if (validation.correctness == Correctness.RIGHT) {
            correctnessMessage = "Right answer ! Congratulations !!";
        } else {
            correctnessMessage = "Wrong answer ! Maybe next time...";
        }
        return new ValidationResponse(
                correctnessMessage,
                validation.missingWord,
                validation.solution
        );
    }

    @RequestMapping("/healthcheck")
    public String healthcheck() {
        return "Ok";
    }

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }
}
