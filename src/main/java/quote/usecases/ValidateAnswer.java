package quote.usecases;

import org.slf4j.LoggerFactory;
import quote.Application;
import quote.domain.*;
import quote.domain.AnswerRepository.AnswerNotFound;

import static quote.domain.Correctness.RIGHT;
import static quote.domain.Correctness.WRONG;

public class ValidateAnswer {
    private AnswerRepository answerRepository;

    public ValidateAnswer(AnswerRepository answerRepository) {
        this.answerRepository = answerRepository;
    }

    public Validation validateAnswer(String id, String answer) throws AnswerNotFound {
        LoggerFactory.getLogger(Application.class).info("ID: "+id);
        LoggerFactory.getLogger(Application.class).info("ANSWER: "+answer);
        Answer foundAnswer = answerRepository.fetchAnswerById(new AnswerId(id));
        Correctness correctness = null;
        if (foundAnswer.missingWord.equalsIgnoreCase(answer)) {
            correctness = RIGHT;
        } else {
            correctness = WRONG;
        }
        return new Validation(
                correctness,
                foundAnswer.missingWord,
                foundAnswer.fullQuote
        );
    }
}
