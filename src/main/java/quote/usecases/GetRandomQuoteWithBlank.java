package quote.usecases;

import quote.domain.*;
import quote.domain.AnswerRepository.PersistFailure;

import java.util.Arrays;
import java.util.Comparator;

public class GetRandomQuoteWithBlank {
    private QuoteRepository quoteRepository;
    private AnswerRepository answerRepository;
    private IdGenerator idGenerator;

    public GetRandomQuoteWithBlank(QuoteRepository quoteRepository, AnswerRepository answerRepository, IdGenerator idGenerator) {
        this.quoteRepository = quoteRepository;
        this.answerRepository = answerRepository;
        this.idGenerator = idGenerator;
    }

    public QuoteWithBlank getRandomQuoteWithBlank() throws PersistFailure {
        Quote quote = quoteRepository.fetchRandom();
        String[] words = quote.content.split("\\W+");
        String workToBeBlanked = Arrays.stream(words).max(Comparator.comparingInt(String::length)).get();
        String blank = "";
        for (int i = 0; i < workToBeBlanked.length(); i++) {
            blank += "_";
        }

        Answer answer = new Answer(
                new AnswerId(idGenerator.next()),
                workToBeBlanked,
                quote.content
        );
        answerRepository.persistAnswer(answer);

        return new QuoteWithBlank(
                answer.id,
                quote.content.replaceFirst(workToBeBlanked, blank)
        );
    }
}
