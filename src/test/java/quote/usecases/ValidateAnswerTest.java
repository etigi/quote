package quote.usecases;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import quote.domain.Answer;
import quote.domain.AnswerId;
import quote.domain.AnswerRepository;
import quote.domain.AnswerRepository.AnswerNotFound;
import quote.domain.Validation;

import static org.junit.Assert.assertEquals;
import static quote.domain.Correctness.RIGHT;
import static quote.domain.Correctness.WRONG;

@RunWith(SpringJUnit4ClassRunner.class)
public class ValidateAnswerTest {
    private AnswerRepository repository = new AnswerRepository() {

        @Override
        public Answer fetchAnswerById(AnswerId id) {
            return new Answer(
                    id,
                    "happens",
                    "Life is what happens when you're busy making other plans."
            );
        }

        @Override
        public void persistAnswer(Answer answer) {
            // do nothing
        }
    };

    ValidateAnswer usecase = new ValidateAnswer(
            repository
    );

    @Test
    public void rightAnswer() throws AnswerNotFound {
        // WHEN
        Validation validation = usecase.validateAnswer("1", "happens");

        // THEN
        assertEquals(validation.correctness, RIGHT);
        assertEquals(validation.missingWord, "happens");
        assertEquals(validation.solution, "Life is what happens when you're busy making other plans.");
    }

    @Test
    public void wrongAnswer() throws AnswerNotFound {
        // WHEN
        Validation validation = usecase.validateAnswer("1", "goes by");

        // THEN
        assertEquals(validation.correctness, WRONG);
        assertEquals(validation.missingWord, "happens");
        assertEquals(validation.solution, "Life is what happens when you're busy making other plans.");
    }
}