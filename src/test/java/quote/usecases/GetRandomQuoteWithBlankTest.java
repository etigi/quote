package quote.usecases;

import quote.domain.*;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.junit.Assert.assertEquals;

@RunWith(SpringJUnit4ClassRunner.class)
public class GetRandomQuoteWithBlankTest {
    @Test
    public void testBlankQuote() throws AnswerRepository.PersistFailure {
        // GIVEN
        GetRandomQuoteWithBlank usecase = new GetRandomQuoteWithBlank(
                () -> new Quote(
                        "Life is what happens when you're busy making other plans."
                ),
                new AnswerRepository() {
                    @Override
                    public Answer fetchAnswerById(AnswerId id) {
                        return null;
                    }

                    @Override
                    public void persistAnswer(Answer answer) {

                    }
                },
                () -> "1"
        );

        // WHEN
        QuoteWithBlank actual = usecase.getRandomQuoteWithBlank();

        // THEN
        assertEquals("Life is what _______ when you're busy making other plans.", actual.content);
        assertEquals("1", actual.id.id);
    }
}