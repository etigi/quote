package quote.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class DBEraser {
    private Connection connection;

    public DBEraser(Connection connection) {
        this.connection = connection;
    }

    public void erase() throws SQLException {
        PreparedStatement pstmt = connection.prepareStatement(
                "TRUNCATE TABLE quote.answer"
        );
        pstmt.execute();
    }
}
