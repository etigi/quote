package quote.db;

import quote.domain.AnswerRepository;
import quote.infrastructure.PostgresAnswerRepository;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DependencyInjectionForDBTests {
    private String dbName;
    private String dbUser;
    private String dbPassword;
    private AnswerRepository answerRepository;
    private Connection connection;
    private DBEraser eraser;

    public DependencyInjectionForDBTests(String dbName, String dbUser, String dbPassword) {
        this.dbName = dbName;
        this.dbUser = dbUser;
        this.dbPassword = dbPassword;
    }

    public AnswerRepository provideAnswerRepository() throws SQLException {
        if (answerRepository == null) {
            answerRepository = new PostgresAnswerRepository(
                    provideConnection()
            );
        }
        return answerRepository;
    }

    private Connection provideConnection() throws SQLException {
        if (connection == null) {
            String dbUrl = "jdbc:postgresql://localhost:5433/"+dbName;
                connection = DriverManager.getConnection(dbUrl, dbUser, dbPassword);
        }
        return connection;
    }

    public DBEraser provideDBEraser() throws SQLException {
        if (eraser == null) {
            eraser = new DBEraser(provideConnection());
        }
        return eraser;
    }
}
