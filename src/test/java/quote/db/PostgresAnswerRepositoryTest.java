package quote.db;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import quote.domain.Answer;
import quote.domain.AnswerId;
import quote.domain.AnswerRepository;
import quote.domain.AnswerRepository.AnswerNotFound;
import quote.domain.AnswerRepository.PersistFailure;

import java.sql.SQLException;

import static org.junit.Assert.assertEquals;

@RunWith(SpringJUnit4ClassRunner.class)
public class PostgresAnswerRepositoryTest {
    private DBEraser eraser;
    private AnswerRepository repository;

    @Before
    public void beforeEarch() throws Exception {
        // TODO Get db credendials from environment ?
        DependencyInjectionForDBTests di = new DependencyInjectionForDBTests(
                "quote",
                "quoteuser",
                "quoteuser"
        );
        repository = di.provideAnswerRepository();
        eraser = di.provideDBEraser();
        eraser.erase();
    }

    @Test
    public void insertAndFetch() throws AnswerNotFound, PersistFailure {
        // GIVEN
        AnswerId id = new AnswerId("1");
        // WHEN
        repository.persistAnswer(
                new Answer(
                        id,
                        "missing word",
                        "full quote"
                )
        );
        Answer answer = repository.fetchAnswerById(id);

        // THEN
        assertEquals(answer.id.id, "1");
        assertEquals(answer.missingWord, "missing word");
        assertEquals(answer.fullQuote, "full quote");
    }

    @Test(expected = PersistFailure.class)
    public void insertTwiceSameId() throws SQLException, PersistFailure {
        // GIVEN
        Answer answer = new Answer(
                new AnswerId("1"),
                "missing word",
                "full quote"
        );
        // WHEN
        repository.persistAnswer(
                answer
        );
        repository.persistAnswer(
                answer
        );

        // THEN
        assertEquals(true, true);
    }

    @Test(expected = AnswerNotFound.class)
    public void answerNotFound() throws AnswerNotFound {
        // WHEN
        repository.fetchAnswerById(new AnswerId("1"));
    }

}
