# Notes

# Introduction
Ce repository sert d'example:
 - à la conteneurisation d'une application Java/Springboot via Docker
 - au test d'intégration end-to-end de l'API via Newman
 - à la conteneurisation d'une base de données postgres
 - à la migration de schémas SQL via Flyway
 - au mock des appels de l'application vers une API externe via Wiremock
 
# Application

L'application expose les endpoints suivant:
 
`GET /` - retourne une citation random dont le mot le plus long est transformé en trou : 
```
{
    "answerId":"9c3c34bf-9f55-468d-95d5-765bb2705b36"
    "quote":"________ you have become, the dark side I sense in you."
}
```

`POST /validate` - à partir d'un `answerId` et une proposition (`answer`), valide une réponse :

Body:
```
{
    "id": "9c3c34bf-9f55-468d-95d5-765bb2705b36",
    "answer": "Strong",
}
``` 

Réponse:
```
{
    "correctness": "Wrong answer ! Maybe next time...",
    "missingword": "Powerful",
    "fullquote": "Powerful you have become, the dark side I sense in you."
}
```

# Schémas
![](./docs/diagramme-sans-mock.png)
![](./docs/diagramme-mock.png)

# 0 - Lancer la base de données de test 
```
env $(cat .env_test) docker-compose -f docker-compose-db-test.yml up
```

# 1 - Builder l'application
```
env $(cat .env) ./gradlew build && mkdir -p build/dependency && (cd build/dependency; jar -xf ../libs/*.jar) && echo "Done"
```

# 2 - Construire l'image docker
```
docker-compose build
```

# 3 AVEC MOCK - Monter/lancer les conteneurs avec mocks des appels externes à l'API de citations
```
env $(cat .env_test) docker-compose up
```

# 3 SANS MOCK - Monter/lancer les conteneurs sans mocks des appels externes à l'API de citations
```
env $(cat .env) docker-compose up
```

# 4 AVEC MOCK - Lancer les tests newman
```
docker-compose run newman run /postman/quote_with_mock.postman_collection.json -e /postman/quote.postman_environment.json
```

# 4 SANS MOCK - Lancer les tests newman
```
docker-compose run newman run /postman/quote_without_mock.postman_collection.json -e /postman/quote.postman_environment.json
```

# TO-DOs 
- Séparer les tâches gradle de tests unitaires et test d'intégration BDD
- Parametriser les credentials de BDD dans les tests d'intégration BDD
- Factoriser la conf Postgress et Flyway entre les deux fichiers docker-compose ?