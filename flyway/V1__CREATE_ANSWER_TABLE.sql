CREATE TABLE answer(
   id VARCHAR (50) PRIMARY KEY,
   missing_word VARCHAR (50),
   full_quote VARCHAR (1024)
);